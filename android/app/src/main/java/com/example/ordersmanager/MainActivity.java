package com.example.ordersmanager;

import android.content.ContentResolver;
import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;

public class MainActivity extends FlutterActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      Uri soundUri = Uri.parse( "android.resource://" +  getApplicationContext().getPackageName() + "/" +  R.raw.audio2);

      AudioAttributes audioAttributes = new AudioAttributes.Builder()
              .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
              .setUsage(AudioAttributes.USAGE_ALARM)
              .setUsage(AudioAttributes.USAGE_NOTIFICATION)
              .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
              .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
              .build();

      // Creating Channel
      NotificationChannel channel = new NotificationChannel("test_1", "notification_sound", NotificationManager.IMPORTANCE_HIGH);
      channel.setSound(soundUri, audioAttributes);

      NotificationManager notificationManager = getSystemService(NotificationManager.class);
      notificationManager.createNotificationChannel(channel);
    }
    GeneratedPluginRegistrant.registerWith(this);
  }
}