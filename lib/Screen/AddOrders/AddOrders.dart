import 'package:flutter/material.dart';
import 'package:orders_manager/theme/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:orders_manager/Components/InputDropdown.dart';
import 'package:image_picker/image_picker.dart';
import 'package:orders_manager/service.dart';
import 'dart:math';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:orders_manager/Components/validations.dart';
import 'dart:async';
import 'package:flutter/services.dart';

const double _kPickerSheetHeight = 216.0;

class AddOrders extends StatefulWidget {
  @override
  _AddOrdersState createState() => _AddOrdersState();
}

class _AddOrdersState extends State<AddOrders> {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _text = TextEditingController();
  bool _isLoading = false;
  DateTime date = DateTime.now();
  var _image;
  String lastSelectedValue;
  String name,phone,price,description;
  DateTime created_at,expiry_date;
  var now = DateTime.now();
  var items;
  String fileName;
  bool uploaded;
  String photoUrl;
  Validations validations = new Validations();
  bool autovalidate = false;
  bool dateValidate = false;

  crudMedthods crudObj = new crudMedthods();

  Future getImageLibrary() async {
    var gallery = await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 700);
    setState(() {
      _image = gallery;
    });
  }

  Future cameraImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera, maxWidth: 700);
    setState(() {
      _image = image;
    });
  }

  @override
  void dispose() {
    _text.dispose();
    super.dispose();
  }


  Widget _buildBottomPicker(Widget picker) {
    return Container(
      height: _kPickerSheetHeight,
      padding: const EdgeInsets.only(top: 6.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: GestureDetector(
          // Blocks taps from propagating to the modal sheet and popping.
          onTap: () {},
          child: SafeArea(
            top: false,
            child: picker,
          ),
        ),
      ),
    );
  }

  void showDemoActionSheet({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() { lastSelectedValue = value; });
      }
    });
  }

  selectCamera () {
    showDemoActionSheet(
      context: context,
      child: CupertinoActionSheet(
          title: const Text('Select Camera'),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: const Text('Camera'),
              onPressed: () {
                Navigator.pop(context, 'Camera');
                cameraImage();
              },
            ),
            CupertinoActionSheetAction(
              child: const Text('Photo Library'),
              onPressed: () {
                Navigator.pop(context, 'Photo Library');
                getImageLibrary();
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            },
          )
      ),
    );
  }

  randomNumber(){
    var rnd = new Random();
    var next = rnd.nextDouble() * 1000000;
    while (next < 100000) {
      next *= 10;
    }
    fileName = next.toInt().toString();
    return fileName;
  }

  submmit(){
    final FormState form = formKey.currentState;
    if (!form.validate() || expiry_date == null || _image == null) {
      dateValidate = true;
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      autovalidate = true; // Start validating on every change.
    }else{
      dateValidate = false;
      form.save();
      print(expiry_date);
      randomNumber();
      uploadFile();
      setState(() {
        Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
      });
    }
  }

  Future uploadFile() async {
    _isLoading = true;
    print(_image);
    if(_image !=null){
      StorageReference reference = FirebaseStorage.instance.ref().child('image').child(fileName);
      StorageUploadTask uploadTask = reference.putFile(_image,StorageMetadata(contentType: 'image/jpeg'));
      var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
      var deleteUrl = await (await uploadTask.onComplete).ref.getPath();
      print(deleteUrl);
        photoUrl = dowurl.toString();
      crudObj.addData({
        'name': name,
        'phone': phone,
        'price': price,
        'description': description,
        'created_at': now,
        'expiry_date': expiry_date,
        'image': photoUrl,
        'deleteImage': deleteUrl,
      }).then((result) {
          _isLoading = false;
          print("Them thanh cong");
      }).catchError((e) {
        print(e);
      });
    }else{
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: new AppBar(
        title: new Text('Thêm Đơn hàng'),
        centerTitle: true,
        backgroundColor: Color(getColorHexFromStr('#FDD148')),
        elevation: 0.0,
      ),
      key: _scaffoldKey,
      body: _isLoading == true ? new Center(
        child: new CupertinoActivityIndicator(),
      ):
      SingleChildScrollView(
        child: new GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
          child: new Container(
            padding: EdgeInsets.all(8.0),
            child: new Form(
              key: formKey,
              autovalidate: autovalidate,
              child: new DropdownButtonHideUnderline(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new Container(
                      child: TextFormField(
                        style: textStyle,
                        validator: validations.validateNumber,
                        decoration: InputDecoration(
                            fillColor: whiteColor,
                            hintText: 'Tên khách hàng',
                            icon: new Icon(Icons.bookmark_border),
                            hintStyle: TextStyle(color: greyColor),
                            counterStyle: textStyle,
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)
                            )
                        ),
                        onSaved: (String valueName){
                          name = valueName;
                        },
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.fromLTRB(0.0, 18.0, 8.0, 0.0),
                      child: TextFormField(
                        style: textStyle,
                        keyboardType: TextInputType.number,
                        maxLength: 10,
                        maxLengthEnforced: false,
                        validator: validations.validateNumber,
                        decoration: InputDecoration(
                            fillColor: whiteColor,
                            hintText: 'Điện thoại',
                            icon: new Icon(Icons.phone),
                            hintStyle: TextStyle(color: greyColor),
                            helperStyle: textStyleValidate,
                            counterStyle: textStyle,
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)
                            )
                        ),
                        onSaved: (String valuePhone){
                          phone = valuePhone;
                        },
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.fromLTRB(0.0, 18.0, 8.0, 0.0),
                      child: new Row(
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.only(right: 15.0),
                            child:  new Icon(Icons.date_range, color: greyColor,),
                          ),
                          new Expanded(
                              flex: 5,
                            child: GestureDetector(
                                onTap: () {
                                  showCupertinoModalPopup<void>(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return _buildBottomPicker(
                                        CupertinoDatePicker(
                                          mode: CupertinoDatePickerMode.dateAndTime,
                                          initialDateTime: date,
                                          onDateTimeChanged: (DateTime newDateTime) {
                                            setState(() {
                                              date = newDateTime;
                                              expiry_date = date;
                                            });
                                          },
                                        ),
                                      );
                                    },
                                  );
                                },
                                child: new InputDropdown(
                                  valueText: DateFormat.yMMMMd().format(date) +' '+ DateFormat.Hm().format(date),
                                  valueStyle: TextStyle(color: blackColor),
                                )
                            )
                          ),
                        ],
                      )
                    ),
                    dateValidate == true ?
                    new Container(
                      padding: EdgeInsets.only(left: 40.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Text("Chọn ngày lấy hàng",style: textStyleActive,),
                        ],
                      )

                    ):new Center(),
                    new Container(
                      padding: EdgeInsets.fromLTRB(0.0, 18.0, 8.0, 0.0),
                      child: TextFormField(
                        style: textStyle,
                        keyboardType: TextInputType.number,
                        validator: validations.validateNumber,
                        decoration: InputDecoration(
                            fillColor: whiteColor,
                            hintText: 'Giá',
                            icon: new Icon(Icons.attach_money),
                            hintStyle: TextStyle(color: greyColor),
                            helperStyle: textStyleValidate,
                            counterStyle: textStyle,
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)
                            )
                        ),
                        onSaved: (String valuePrice){
                          price = valuePrice;
                        },
                      ),
                    ),

                    new Container(
                      padding: EdgeInsets.fromLTRB(40.0, 18.0, 8.0, 0.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Expanded(
                            flex:3,
                            child: new Container(
                              child: new Text('Ảnh mô tả',style: textGrey,),
                            ),
                          ),
                           new ClipRRect(
                                borderRadius: new BorderRadius.circular(100.0),
                                child:_image == null
                                    ? new Container(
                                    height: 150.0,
                                    width: 150.0,
                                    color: Color(getColorHexFromStr('#FDD148')),
                                    child: new IconButton(
                                        icon: new Icon(Icons.add_a_photo,size: 50.0,color: whiteColor,),
                                        onPressed: () {selectCamera();}
                                    )
                                ): new GestureDetector(
                                  onTap: () {selectCamera();},
                                  child: Image.file(_image,fit: BoxFit.cover, height: 150.0,width: 150.0,),
                                )
                            ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.fromLTRB(0.0, 18.0, 8.0, 0.0),
                      child: new Container(
                        child:TextFormField(
                          style: textStyle,
                          maxLines: 3,
                          validator: validations.validateNumber,
                          decoration: InputDecoration(
                              fillColor: whiteColor,
                              hintText: 'Nội dung chi tiết',
                              icon: new Icon(Icons.note),
                              hintStyle: TextStyle(color: greyColor),
                              counterStyle: textStyle,
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)
                              )
                          ),
                          onSaved: (String valueName){
                            description = valueName;
                          },
                        ),
                      )
                    ),
                    new Container(
                      padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 30.0),
                      child: ButtonTheme(
                        minWidth: screenSize.width,
                        height: 45.0,
                        child: RaisedButton.icon(
                          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                          elevation: 0.0,
                          color: primaryColor,
                          icon:  _isLoading == true ? CupertinoActivityIndicator() :new Text(''),
                          label: new Text('THÊM ĐƠN HÀNG',style: heading18),
                          onPressed:(){
                            submmit();
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      )
    );
  }
}
