import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:orders_manager/theme/style.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:orders_manager/service.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';


class OrdersDetail extends StatefulWidget {
  final DocumentSnapshot resultsDetail;
  OrdersDetail({this.resultsDetail});

  @override
  _OrdersDetailState createState() => _OrdersDetailState();
}

class _OrdersDetailState extends State<OrdersDetail> {
  var dayExpiry;
  var hourexpiry;
  var dayCreated;
  var hourCreated;
  crudMedthods crudObj = new crudMedthods();
  bool _isLoading = false;

  String phone;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timestamp a = widget.resultsDetail.data['expiry_date'];
    dayExpiry = DateFormat.yMMMMd().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));
    hourexpiry = DateFormat.Hm().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));

    Timestamp created = widget.resultsDetail.data['created_at'];
    dayCreated = DateFormat.yMMMMd().format(DateTime.fromMicrosecondsSinceEpoch(created.millisecondsSinceEpoch*1000));
    hourCreated = DateFormat.Hm().format(DateTime.fromMicrosecondsSinceEpoch(created.millisecondsSinceEpoch*1000));

    phone =  widget.resultsDetail.data['phone'].toString();
  }

  handleDelete(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            title: new Text("Thông báo"),
            content: new Text("Bạn có chắc chắn muốn xóa không"),
            actions: <Widget>[
              new FlatButton(
                  onPressed: (){Navigator.pop(context);},
                  child: new Text('CANCEL',style: textGrey,)),
              new FlatButton(
                  onPressed: (){deleteOrders(widget.resultsDetail);Navigator.pop(context);},
                  child: new Text('OK')),

            ],
          );
        }
    );
  }

  deleteOrders(DocumentSnapshot id) async {
    _isLoading = true;
    final FirebaseStorage storage = FirebaseStorage(storageBucket: 'gs://firestorecrud-d732d.appspot.com/');
    await storage.ref().child(id.data['deleteImage']).delete();
    crudObj.deleteData(id.documentID);
    setState(() {
      _isLoading = false;
      Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: primaryColor,
        title: new Text("Chi tiết"),
      ),
      body: _isLoading == true ? new Center(
        child: new CupertinoActivityIndicator()
      ): new SingleChildScrollView(
        child: new Container(
          padding: EdgeInsets.all(8.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Center(
                child: new SizedBox(
                  height: 230.0,
                  child: new Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(
                            color: Colors.white,
                            style: BorderStyle.solid,
                            width: 3.0),
                        image: DecorationImage(
                          image: CachedNetworkImageProvider(widget.resultsDetail.data['image'].toString()),fit: BoxFit.cover),
                    ),
                  ),
                ),
              ),
              new Container(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: new Row(
                  children: <Widget>[
                    new Text('Ngày đặt hàng:',style: textStyle,),
                    Text('$dayCreated / $hourCreated',
                      style: textBoldBlack,
                    ),
                  ],
                ),
              ),
              new Container(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: new Row(
                  children: <Widget>[
                    new Text('Ngày Lấy hàng hàng:',style: textStyle,),
                    Text('$dayExpiry / $hourexpiry',
                      style: textBoldBlack,
                    ),
                  ],
                ),
              ),
              new Container(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: new Container(
                  child: new Row(
                    children: <Widget>[
                      new Icon(Icons.attach_money,
                        color: greenColor,
                      ),
                      Text(widget.resultsDetail.data['price'],
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                            color: Colors.green ),
                      ),
                    ],
                  ),
                ),
              ),
              new Container(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Text(widget.resultsDetail.data['name'],
                  style: headingBlack,
                ),
              ),
              new Container(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: new InkWell(
                  onTap: () => launch('tel://$phone', forceSafariVC: false),
                  child: Text(widget.resultsDetail.data['phone'],
                    style: headingLink,
                  ),
                ),
              ),
              new Container(
                height: 100.0,
                child: new SingleChildScrollView(
                  child: Text(widget.resultsDetail.data['description'],
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        color: Colors.grey),
                  ),
                ),
              ),
              new Center(
                child: new ButtonTheme(
                  height: 50.0,
                  child: RaisedButton.icon(
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                    onPressed: () {handleDelete(context);},
                    elevation: 0.5,
                    color: redColor,
                    icon: new Icon(Icons.restore_from_trash),
                    label: new Text('Xoá Đơn Hàng'),
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}