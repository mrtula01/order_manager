import 'package:flutter/material.dart';
import 'package:orders_manager/theme/style.dart';
import 'package:orders_manager/Components/itemOrders.dart';
import 'package:orders_manager/Screen/AddOrders/AddOrders.dart';
import 'package:orders_manager/service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:orders_manager/Screen/OrdersDetail/OrdersDetail.dart';
import 'package:firebase_messaging/firebase_messaging.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  crudMedthods crudObj = new crudMedthods();
  var items;
  PageController _pageController;
  int _page = 0;
  var now = DateTime.now().millisecondsSinceEpoch;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    crudObj.getData().then((results){
      items = results;
    });
    setupFirebaseMes();
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  setupFirebaseMes(){
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
//        _showItemDialog(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
//        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
//        _navigateToItemDetail(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
//      setState(() {
//        _homeScreenText = "Push Messaging token: $token";
//      });
      print(token);
    });
  }

  void navigationTapped(int page) {
    // Animating to the page.
    // You can use whatever duration and curve you like
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  navigateToDetail(DocumentSnapshot resultsDetail){
    print('tap');
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => OrdersDetail(resultsDetail: resultsDetail)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Danh sách đơn hàng"),
        backgroundColor: primaryColor,
        elevation: 0.0,
      ),
      floatingActionButton: new FloatingActionButton(
        backgroundColor: primaryColor,
        onPressed: (){
          Navigator.of(context).push(new MaterialPageRoute<Null>(
              builder: (BuildContext context) {
                return new AddOrders();
              },
              fullscreenDialog: true));
        },
        child: new Icon(Icons.add),
      ),
      body: new PageView(
          children: [
            tabHome(context),
            tab2(context),
            tabExpiry(context),
          ],
          onPageChanged: onPageChanged,
          controller: _pageController
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: navigationTapped, // new
        currentIndex: _page,
        fixedColor: primaryColor,// new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Tất cả'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.trending_up),
            title: Text('Sắp lấy'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.history),
            title: Text('Hết hạn'),
          ),
        ],
      ),
    );
  }

  Widget tabHome(BuildContext contex){
    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('orders').orderBy("created_at", descending: true).snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Center(
            child: const CupertinoActivityIndicator(),
          );
          return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context,index){
                Timestamp a = snapshot.data.documents[index].data['expiry_date'];
                var expiry_date = a.millisecondsSinceEpoch - now;
                var day = DateFormat.yMMMMd().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));
                var hour = DateFormat.Hm().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));

                return Column(
                  children: <Widget>[
                    new InkWell(
                      onTap: () => navigateToDetail(snapshot.data.documents[index]),
                      child: ItemOrders(
                        itemName: snapshot.data.documents[index].data['name'],
                        description: snapshot.data.documents[index].data['description'],
                        imgPath: snapshot.data.documents[index].data['image'],
                        price: snapshot.data.documents[index].data['price'] != null ? snapshot.data.documents[index].data['price'] : '',
                        dateEnd: day,
                        hourEnd: hour,
                        statusOrder: (expiry_date < 172800000 && a.millisecondsSinceEpoch > now)
                            ? 'Sắp lấy' :(a.millisecondsSinceEpoch < now) ? 'Hết hạn' :'Mới',
                        colorStatus: (expiry_date < 172800000 && a.millisecondsSinceEpoch > now)
                            ? redColor :(a.millisecondsSinceEpoch < now) ? grayColor :primaryColor,
                      ),
                    )
                  ],
                );

              }
          );
        }
    );
  }

  Widget tab2(BuildContext context){
    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('orders').orderBy("created_at", descending: true).snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Center(
            child: const CupertinoActivityIndicator(),
          );
          return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context,index){
                Timestamp a = snapshot.data.documents[index].data['expiry_date'];
                var expiry_date = a.millisecondsSinceEpoch - now;
                var day = DateFormat.yMMMMd().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));
                var hour = DateFormat.Hm().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));
                return (expiry_date < 172800000 && a.millisecondsSinceEpoch > now) ?
                  Column(
                  children: <Widget>[
                    new InkWell(
                      onTap: () => navigateToDetail(snapshot.data.documents[index]),
                      child: ItemOrders(
                        itemName: snapshot.data.documents[index].data['name'],
                        description: snapshot.data.documents[index].data['description'],
                        imgPath: snapshot.data.documents[index].data['image'],
                        price: snapshot.data.documents[index].data['price'] != null ? snapshot.data.documents[index].data['price'] : '',
                        dateEnd: day,
                        hourEnd: hour,
                        statusOrder: (expiry_date < 172800000 && a.millisecondsSinceEpoch > now)
                            ? 'Sắp lấy' :(a.millisecondsSinceEpoch < now) ? 'Hết hạn' :'Mới',
                        colorStatus: (expiry_date < 172800000 && a.millisecondsSinceEpoch > now)
                            ? redColor :(a.millisecondsSinceEpoch < now) ? grayColor :primaryColor,
                      ),
                    )
                  ],
                ):new Center();
              }
          );
        }
    );
  }

  Widget tabExpiry(BuildContext context){
    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('orders').orderBy("created_at", descending: true).snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Center(
            child: const CupertinoActivityIndicator(),
          );
          return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context,index){
                Timestamp a = snapshot.data.documents[index].data['expiry_date'];
                var expiry_date = a.millisecondsSinceEpoch - now;
                var day = DateFormat.yMMMMd().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));
                var hour = DateFormat.Hm().format(DateTime.fromMicrosecondsSinceEpoch(a.millisecondsSinceEpoch*1000));
                return (a.millisecondsSinceEpoch < now) ?
                Column(
                  children: <Widget>[
                    new InkWell(
                      onTap: () => navigateToDetail(snapshot.data.documents[index]),
                      child: ItemOrders(
                        itemName: snapshot.data.documents[index].data['name'],
                        description: snapshot.data.documents[index].data['description'],
                        imgPath: snapshot.data.documents[index].data['image'],
                        price: snapshot.data.documents[index].data['price'] != null ? snapshot.data.documents[index].data['price'] : '',
                        dateEnd: day,
                        hourEnd: hour,
                        statusOrder: (expiry_date < 172800000 && a.millisecondsSinceEpoch > now)
                            ? 'Sắp lấy' :(a.millisecondsSinceEpoch < now) ? 'Hết hạn' :'Mới',
                        colorStatus: (expiry_date < 172800000 && a.millisecondsSinceEpoch > now)
                            ? redColor :(a.millisecondsSinceEpoch < now) ? grayColor :primaryColor,
                      ),
                    )
                  ],
                ):new Center();
              }
          );
        }
    );
  }
}
