import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

class crudMedthods {
  Future<void> addData(data) async{
    Firestore.instance.collection('orders').add(data).catchError((e){
      print(e);
    });
  }

  getData() async {
    return await Firestore.instance.collection('orders').getDocuments();
  }

  deleteData(docId){
    Firestore.instance.collection('orders').document(docId).delete().catchError((e){
      print(e);
    });
  }
}