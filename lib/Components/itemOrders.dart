import 'package:flutter/material.dart';
import 'package:orders_manager/theme/style.dart';
import 'package:cached_network_image/cached_network_image.dart';


class ItemOrders extends StatelessWidget {
  String imgPath, itemName,description, price,dateEnd,hourEnd, statusOrder;
  VoidCallback onTap;
  Color colorStatus;

  ItemOrders({this.imgPath,this.itemName,this.description, this.price, this.dateEnd,this.hourEnd, this.onTap, this.statusOrder, this.colorStatus});

  @override
  Widget build(BuildContext context) {
    return Padding(
          padding: EdgeInsets.all(8.0),
          child: Stack(
            children: <Widget>[
              Material(
                  borderRadius: BorderRadius.circular(10.0),
                  elevation: 3.0,
                  child: Container(
                      padding: EdgeInsets.only(left: 15.0, right: 10.0),
                      width: MediaQuery.of(context).size.width - 20.0,
                      height: 150.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
//                      image: DecorationImage(image: AssetImage('assets/bg2.jpg')),
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 3,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(100, 10, 10, 10),
                              height: 125.0,
                              width: 125.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: CachedNetworkImageProvider(imgPath),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          new Expanded(
                              flex: 8,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Row(
                                    children: <Widget>[
                                      new Container(
                                        padding: EdgeInsets.only(left: 20.0),
                                        height: 30.0,
                                        width: 30.0,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(37.5),
                                            border: Border.all(
                                                color: Colors.grey,
                                                style: BorderStyle.solid,
                                                width: 2.0),
                                            image: DecorationImage(image: AssetImage('assets/chris.jpg'))),
                                      ),
                                      new Container(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child:  Text(itemName != null ? itemName : '',
                                          style: TextStyle(
                                              fontFamily: 'Quicksand',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 13.5,
                                              color: Colors.black),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 7.0),
                                  Row(
                                    children: <Widget>[
                                      new Container(
                                        padding: EdgeInsets.only(left: 15.0, right: 0.0),
                                        width: 240,
                                        child:   Text(description != null ? description : '',
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.justify,
                                          maxLines: 2,
                                          style: TextStyle(
                                              fontFamily: 'Quicksand',
                                              fontWeight: FontWeight.w100,
                                              fontSize: 14.0,
                                              color: Colors.black),
                                        ),
                                      ),
                                    ],
                                  ),

                                  new Container(
                                    padding: EdgeInsets.only(top: 10.0),
                                    alignment: Alignment.topLeft,
                                    child: new Row(
                                      children: <Widget>[
                                        new Expanded(
                                          flex: 4,
                                          child: Container(
                                            padding: EdgeInsets.only(left: 11.0, right: 3.0),
                                            child:  Text('\$ $price',
                                              style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20.0,
                                                  color: Colors.green ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 11.0,top: 10),
                                    child: new Row(
                                      children: <Widget>[
                                        new Expanded(
                                            flex: 3,
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  child: new Icon(Icons.date_range,
                                                    color:Colors.black,
                                                    size: 15,
                                                  ),
                                                ),
                                                //Date
                                                new Container(
                                                  child:  Text(dateEnd != null ? dateEnd : '',
                                                    style: TextStyle(
                                                        fontFamily: 'Quicksand',
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 13.5,
                                                        color: Colors.black),
                                                  ),
                                                ),
                                              ],
                                            )
                                        ),
                                        new Expanded(
                                            flex: 1,
                                            child: new Row(
                                              children: <Widget>[
                                                //Date
                                                new Container(
                                                  alignment: Alignment.bottomRight,
                                                  child: new Row(
                                                    children: <Widget>[
                                                      new Text(hourEnd,
                                                        style: TextStyle(
                                                            fontFamily: 'Quicksand',
                                                            fontWeight: FontWeight.bold,
                                                            fontSize: 14.0,
                                                            color: Colors.black),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            )
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              )
                          ),
                        ],
                      )
                  )
              ),
              new Positioned(
                top: 10.0,
                right: 5.0,
                child: new Container(
                  height: 25.0,
                  width: 80.0,
                  decoration: BoxDecoration(
                    color: colorStatus,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: new Center(
                    child: new Text(statusOrder,style: textBoldWhite,),
                  )
                )
              )
            ],
          )
    );
  }
}
