import 'package:flutter/material.dart';
import 'package:orders_manager/Screen/Home/home.dart';


class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({ WidgetBuilder builder, RouteSettings settings })
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    if (animation.status == AnimationStatus.reverse)
      return super.buildTransitions(context, animation, secondaryAnimation, child);
    return FadeTransition(opacity: animation, child: child);
  }
}

class Routes {
  Routes() {
    runApp(new MaterialApp(
      title: "Booking Designer",
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/': return new MyCustomRoute(
            builder: (_) => new HomeScreen(),
            settings: settings,
          );
          case '/home': return new MyCustomRoute(
            builder: (_) => new HomeScreen(),
            settings: settings,
          );
          case '/login': return new MyCustomRoute(
            builder: (_) => new HomeScreen(),
            settings: settings,
          );

        }
        assert(false);
      },
      debugShowCheckedModeBanner: false,
      //home: new SplashScreen(),
//      theme: appTheme,
      //routes: routes,
    ));
  }
}